function onload() {
  const recipients_table = document.querySelector("#recipients-table");
  const donors_table = document.querySelector("#donors-table");

  for (let i = 0; i < 5; i++) {
    let row = document.createElement("tr");

    let cell0 = row.insertCell(0);
    cell0.innerHTML = i + 1;
    for (var j = 1; j < 5; j++) {
      let cell = row.insertCell(j);
      cell.innerHTML = "data" + j;
    }

    recipients_table.appendChild(row);
  }

  for (let i = 0; i < 5; i++) {
    let row = document.createElement("tr");

    let cell0 = row.insertCell(0);
    cell0.innerHTML = i + 1;
    for (var j = 1; j < 5; j++) {
      let cell = row.insertCell(j);
      cell.innerHTML = "data" + j;
    }

    donors_table.appendChild(row);
  }
}
onload();
